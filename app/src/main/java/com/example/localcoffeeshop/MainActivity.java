package com.example.localcoffeeshop;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView idView;
    EditText nameBox;
    EditText ageBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        idView = (TextView) findViewById(R.id.dogID);
        nameBox = (EditText) findViewById(R.id.dogName);
        ageBox = (EditText) findViewById(R.id.dogAge);
    }

    public void addDog(View view) {
        DogDBHandler dbHandler = new DogDBHandler(MainActivity.this);
        int age = Integer.parseInt(ageBox.getText().toString());
        Dog dog = new Dog(age, nameBox.getText().toString());
        dbHandler.addDog(dog);
        nameBox.setText("");
        ageBox.setText("");
    }

    public void searchDog(View view) {
        DogDBHandler dbHandler = new DogDBHandler(MainActivity.this);
        Dog dog = dbHandler.searchDog(nameBox.getText().toString());
        if (dog != null) {
            idView.setText(String.valueOf(dog.getID()));
            ageBox.setText(String.valueOf(dog.getAge()));
        } else {
            idView.setText("Dog not found.");
        }
    }

    public void deleteDog(View view) {
        DogDBHandler dbHandler = new DogDBHandler(MainActivity.this);
        boolean result = dbHandler.deleteDog(nameBox.getText().toString());
        if (result)
        {
            idView.setText("Dog Deleted");
            nameBox.setText("");
            ageBox.setText("");
        }
        else
            idView.setText("Dog not found.");
    }

}

