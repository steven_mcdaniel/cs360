package com.example.localcoffeeshop;

public class Dog {

    // instance variables
    private int var_id;
    private int var_age;
    private String var_name;

    // empty constructor
    public Dog() { }

    // constructor with all three variables
    public Dog(int id, int age, String name) {
        this.var_id = id;
        this.var_age = age;
        this.var_name = name;
    }

    // constructor without id
    public Dog(int age, String name) {
        this.var_age = age;
        this.var_name = name;
    }

    // setters (mutators)
    public void setID(int id) { this.var_id = id; }
    public void setName(String name) { this.var_name = name; }
    public void setAge(int age) { this.var_age = age; }

    // getters (accessors)
    public int getID() { return this.var_id; }
    public String getName() { return this.var_name; }
    public int getAge() { return this.var_age; }
}
